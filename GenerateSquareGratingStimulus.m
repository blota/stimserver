function [mfGrating, cmfLocationDeg] = GenerateSquareGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees)

% GenerateSquareGratingStimulus - Compute an upright square grating for use as a texture
%
% Usage: [mfGrating, cmfLocationDeg] = GenerateSquareGratingStimulus(vnSize, fCyclesPerPixel)
%        [mfGrating, cmfLocationDeg] = GenerateSquareGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree <, fBarWidthDegrees>)
%
% 'vnSize' is a two-elelment vector [N M], defining the size of the grating
% texture to be NxM (in degrees, if 'fPixelsPerDegree' is supplied.
% 'fCyclesPerPixel' or 'fCyclesPerDegree' and 'fPixelsPerDegree' define the
% spatial frequency of the grating.
%
% 'mfGrating' will be a matlab matrix containing a grating stimulus.  The black
% and white values will be 0 (black) and 1 (white)
%
% Make it big enough that you can shift it around on the screen without seeing
% the edges of the texture.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 25th Auust, 2010

% -- Defaults

DEF_fPixelsPerDegree = 1;


% -- Check arguments

if (nargin < 2)
   disp('*** GenerateSquareGratingStimulus: Incorrect usage');
   help GenerateSquareGratingStimulus;
   return;
end

if (~exist('fPixelsPerDegree', 'var') || isempty(fPixelsPerDegree))
   fPixelsPerDegree = DEF_fPixelsPerDegree;
end

if (~exist('fBarWidthDegrees', 'var') || isempty(fBarWidthDegrees))
   % - Default to a 0.5 duty-cycle
   fBarWidthDegrees = 0.5/fCyclesPerDegree;
end

vnSize = round(vfSizeDegrees * fPixelsPerDegree);


% -- Make the grating

fPixelsPerFlip = (fPixelsPerDegree / fCyclesPerDegree);
fBarWidthPixels = fBarWidthDegrees .* fPixelsPerDegree;

% - Make sure texture width is a multiple of the bar period
vnSize(2) = ceil(vnSize(2) + mod(vnSize(2), fPixelsPerFlip));
vfGrating = zeros(1, vnSize(2));

% - Determine bar start and stop pixels
vnStarts = (1:fPixelsPerFlip:vnSize(2)) + fPixelsPerFlip/2 - fBarWidthPixels/2;
vnStarts = vnStarts(vnStarts >= 1);
vnStarts = vnStarts(vnStarts <= vnSize(2));
vnEnds = round(vnStarts + fBarWidthPixels);
vnEnds(vnEnds > vnSize(2)) = vnSize(2);
vnStarts = round(vnStarts);

% - Reshape to start/end pairs
mnBars = [vnStarts' vnEnds'];
nNumBars = size(mnBars, 1);

% - Fill bars with "white"
for (nBar = 1:nNumBars)
   vfGrating(mnBars(nBar, 1):mnBars(nBar, 2)) = 1;
end

% - Trim grating
vfGrating = vfGrating(1:vnSize(2));

% - Replicate base bar to make grating
mfGrating = repmat(vfGrating, vnSize(1), 1);

% - Should we construct location matrices?
if (nargout > 1)
   vfRow = linspace(-vnSize(1)/2, vnSize(1)/2, vnSize(1)) ./ fPixelsPerDegree;
   vfCol = linspace(-vnSize(2)/2, vnSize(2)/2, vnSize(2)) ./ fPixelsPerDegree;
   [cmfLocationDeg{1}, cmfLocationDeg{2}] = ndgrid(vfRow, vfCol);
end


% --- END of GenerateSquareGrating.m ---
