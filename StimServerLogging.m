function [bSuccess] = StimServerLogging(oLogging)

% StimServerLogging - FUNCTION Set logging parameters for the stimulus server
%
% Usage: [bSuccess] = StimServerLogging([])
%        [bSuccess] = StimServerLogging(strLogfile)
%        [bSuccess] = StimServerLogging({strLogAddress nPortNumber <vnTerminator>})

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_sState; %#ok<NUSED>


% -- Check server status

if (isempty(STIMSERV_sState) || ~isfield(STIMSERV_sState, 'oLogging'))
   SSlog('*** StimServerLogging: Stimulus server not running cleanly.\n');
   bSuccess = false;
   return;
end

% - Record current logging status
oOldLogging = STIMSERV_sState.oLogging;

% - Configure new logging details
if (isempty(oLogging))
   % - Turn off logging
   if (~isempty(oOldLogging))
      SSlog('\n');
      SSlog('\n');
      SSlog('\n');
      SSlog('   Closing logging interface.\n');
      SSlog('=================================================================\n');
      
      if (numel(STIMSERV_sState.oLogging) == 2 && STIMSERV_sState.bUsePNET)
         pnet(STIMSERV_sState.hLogSocket, 'close');
      else
         fclose(STIMSERV_sState.hLogSocket);
      end
      
      STIMSERV_sState.hLogSocket = [];
      STIMSERV_sState.oLogging = [];
   end
   
   bSuccess = true;

elseif (ischar(oLogging))
    % - Close old logging interface
    StimServerLogging([]);
    
    % - Try to open a log file with this name
   try
      STIMSERV_sState.hLogSocket = fopen(oLogging, 'a+');
      STIMSERV_sState.oLogging = oLogging;
      bSuccess = true;
      
   catch errMsg
      % - We couldn't open the log file
      SSlog('*** StartStimulusServer: Could not open log file [%s].\n', oLogging);
      SSlog('       Error message: %s\n', errMsg.message);

      % - Try to restore old logging details
      StimServerLogging(oOldLogging);
      SStalkback('SST FAILED');
      bSuccess = false;
   end
   
elseif (iscell(oLogging) && (numel(oLogging) > 1))
    % - Close old logging interface
    StimServerLogging([]);
    
    % - Try to open an UDP or TCP connection with the desired details
   
   if (STIMSERV_sState.bUsePNET)
      % - Open a connection with PNET
      if (STIMSERV_sState.bUseTCP)
         % - Make a TCP connection
         STIMSERV_sState.hLogSocket = pnet('tcpconnect', oLogging{1}, oLogging{2});
         strType = 'TCP';
      else
         % - Make a UDP connection with a random source port
         STIMSERV_sState.hLogSocket = pnet('udpsocket', round(rand*50000) + 1024);
         pnet(STIMSERV_sState.hLogSocket, 'udpconnect', oLogging{1}, oLogging{2});
         strType = 'UDP';
      end
      
      STIMSERV_sState.oLogging = oLogging;
      bSuccess = true;
      
      % - Could a connection be made?
      if (STIMSERV_sState.hLogSocket == -1)
         SSlog('--- StimServerLogging: Warning Could not open PNET %s connection for logging, to [%s : %d].\n', strType, oLogging{1}, oLogging{2});

         SStalkback('SST LOGGING NO CONNECTION');
      end
      
   else
       % - Use the intrument control toolbox
      try
         STIMSERV_sState.hLogSocket = udp(oLogging{1}, oLogging{2});
         fopen(STIMSERV_sState.hLogSocket);
         STIMSERV_sState.oLogging = oLogging;
         strType = 'UDP';
         bSuccess = true;
         
      catch errMsg
         % - We couldn't start the logging connection
         SSlog('*** StimServerLogging: Could not open UDP connection for logging, to [%s : %d].\n', oLogging{1}, oLogging{2});
         SSlog('       Error message: %s\n', errMsg.message);
         
         % - Try to restore old logging details
         StimServerLogging(oOldLogging);
         SStalkback('SST FAILED');
         bSuccess = false;
      end
   end
   
   % - Should we override the current string terminator?
   if (numel(oLogging) > 2)
      STIMSERV_sState.vnLogTerminator = oLogging{3};
   end
   
else
   % - The logging command was incorrectly formed
   SSlog('*** StimServerLogging: Incorrect semantics for logging specification.\n');
   SStalkback('SST CMDERROR');
   bSuccess = false;
end

% - Send an initial log message
if (~isempty(oLogging) && bSuccess)
   SSlog('\n');
   SSlog('\n');
   SSlog('\n');
   SSlog('=================================================================\n');
   
   if (iscell(oLogging))
      SSlog('   Initialised logging interface %s [%s:%d].\n', strType, oLogging{1}, oLogging{2});
   else
      SSlog('   Initialised logging interface %s [%s].\n', 'FILE', oLogging);
   end
   SSlog('\n');
end

% --- END of StimServerLogging.m ---

