function [sStimulus] = STIM_BandLimitedNoise(varargin)

% STIM_BandLimitedNoise - Stimulus object
%
% Usage: [sStimulus] = STIM_BandLimitedNoise( tFrameDuration, fPPD, nNumRepeats, ...
%                                             vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
%                                             vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
%                                             <tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed>)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 12th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, fPPD, nNumRepeats, ...
	fSpatFreqCutoffCPD, fTempFreqCutoffHz, ...
   vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
   tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, fPPD, nNumRepeats};
cStimParams = {fSpatFreqCutoffCPD, fTempFreqCutoffHz, ...
   vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
   tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Make "get duration" function
fhDurationFunction = @(s, b)(s.cStimulusArguments{4}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentBandLimitedNoiseStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeBandLimitedNoiseStimulus);


function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));


            
% --- END of STIM_BandLimitedNoise.m ---
