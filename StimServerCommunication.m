function StimServerCommunication

% StimServerCommunication - Communication protocol for the Stimulus Server
%
% This file describes the stimulus server commands and their semantics
% 
% SS PRESENT nStimID tPreBlankTime(seconds) 
% SS PRESENT nStimID tPreBlankTime(seconds) nNumExtraPresentArgs nNumExtraStimArgs ...
%
% SS DESCRIBE_CONFIG
%     Currently succeeds without performing any action (null command)
% 
% SS DESCRIBE_STIMULI
%     Sends a detailed description of all configured stimuli and their
%     parameters over the talkback interface
%
% SS KILL
%     Tries to kill stimulus presentation and stops server as quickly as
%     possible
% 
% SS SHUTDOWN
%     Stop the StimServer as cleanly as possible
% 
% SS LISTEN_PORT nPortNumber
%     Instructs StimServer to change the network port on which it listens
%     for commands. Following a successful LISTEN_PORT command, no further
%     commands will be accepted over the old command interface
% 
% SS TALKBACK_ADDRESS strAddress nPortNumber
%     Instructs StimServer to make a connection to machine:port for the
%     talkback interface
%
% SS LOGGING strFilename
%     Instructs StimServer to log all commands and output to a text file,
%     stored on the StimServer machine
%
% SS LOGGING strAddress nPortNumber
%     Instructs StimServer to send all logging output over a network
%     interface to machine:port
%
% ------------------- Talkback semantics --------------
%
% SST HELLO
%     Initial talkback interface is up
% 
% SST RUNNING fVersion
%     Indicates that the StimServer is currently running, and indicates the
%     server version
%
% SST LISTEN nListenPort
%     Reports the port number on which StimServer is listening for commands
%
% SST IDLE
%     Reports that the StimServer is currently idle
%
% SST PRESENT nStimID
%     Feedback that the indicated stimulus ID is being presented. Is sent
%     just before the stimulus presentation function is called
%
% SST SEQ nSeqStimID
%     Feedback that the indicated sequence ID of a stimulus sequence is
%     being presented. Is sent just before the stimulus presentation
%     function for the given sequence ID is called
%
% SST SHUTDOWN
%     Reports that the StimServer is shutting down
%
% SST ABORT
%     Reports that the StimServer is quitting abnormally
%
% SST CMDERROR
%    The command was invalid
%
% SST FAILED
%    The command failed

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

help StimServerCommunication;

% --- END of StimServerCommunication.m ---
