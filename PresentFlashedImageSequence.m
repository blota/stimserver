function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentFlashedImageSequence(  hWindow, vtBlankTime, ...
                                          tFlashDuration, nNumRepeats, ...
                                          tfImageStack, strStackIdentifier, bInvert, vfDisplaySizeDeg, ...
                                          vfCentreOffsetDeg, vfPixelsPerDegree, ...
                                          bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
                                          fContrast, vnOrdering)

% PresentFlashedImageSequence - FUNCTION Present a sequence of flashed images
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentFlashedImageSequence(hWindow, vtBlankTime, ...
%                                         tFlashDuration, nNumRepeats, ...
%                                         tfImageStack, strStackIdentifier, bInvert, vfDisplaySizeDeg, ...
%                                         vfCentreOffsetDeg, vfPixelsPerDegree, ...
%                                         < bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
%                                         fContrast, vnOrdering >)
%

% -- Persistent arguments

persistent PFIS_CACHE PFIS_sImageStackCache;


% -- Blank time

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



% -- Check arguments

if (isscalar(vfPixelsPerDegree))
   vfPixelsPerDegree = vfPixelsPerDegree([1 1]);
end

if (~exist('fContrast', 'var'))
   fContrast = 1;
end

if (isempty(vfCentreOffsetDeg))
   vfCentreOffsetDeg = [0 0];
end

if (~exist('vnOrdering', 'var') || isempty(vnOrdering) || all(isnan(vnOrdering)))
   vnOrdering = 1:size(tfImageStack, 3);
end

% - Do we need to re-generate the stimulus?
sStimParams.vnStackDims = size(tfImageStack);
sStimParams.bInvert = bInvert;
sStimParams.fContrast = fContrast;
sStimParams.strStackIdentifier = strStackIdentifier;

[nCacheNum, bCacheHit, bCacheCollision, sCachedParams, PFIS_CACHE] = ...
   ParamCache(sStimParams, PFIS_CACHE);

bGenerateStim = ~bCacheHit | bCacheCollision;

if (bGenerateStim)
   PFIS_sImageStackCache{nCacheNum}.tfImageStack = tfImageStack;
   PFIS_sImageStackCache{nCacheNum}.vnTexIDs = [];
end

% - Create textures, if necessary
if (~isfield(PFIS_sImageStackCache{nCacheNum}, 'vnTexIDs') || ...
    isempty(PFIS_sImageStackCache{nCacheNum}.vnTexIDs))
   bGenerateTexture = true;

else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PFIS_sImageStackCache{nCacheNum}.vnTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Get screen size and colour indices
vnScreenRect = Screen('Rect', hWindow');

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);

if (nGrey == nWhite)
   nGrey = (nWhite + nBlack)/2;
end

% - Generate textures
if (bGenerateTexture)
   % - Replace NaNs with grey
   tfImageStack(isnan(tfImageStack)) = 0.5;
   
   tfImageStack = (tfImageStack-0.5) .* fContrast + 0.5;
   tfImageStack = tfImageStack .* nWhite;
   tfImageStack(tfImageStack<nBlack) = nBlack;
   tfImageStack(tfImageStack>nWhite) = nWhite;
   PFIS_sImageStackCache{nCacheNum}.tfImageStack = tfImageStack;
   
   % - Generate the textures
   PFIS_sImageStackCache{nCacheNum}.vnTexIDs = GenerateTextures(tfImageStack, hWindow);
   
   % - Generate textures for inverted images too
   if (bInvert)
      PFIS_sImageStackCache{nCacheNum}.vnInvertTexIDs = GenerateTextures(nWhite - tfImageStack, hWindow);
   end
end


% -- Generate a mask texture

if (bDrawMask)
   nMaskWidth = fMaskDiameterDeg * vfPixelsPerDegree(1);
   nMaskHeight =  fMaskDiameterDeg * vfPixelsPerDegree(2);
   mbMask = Ellipse(nMaskWidth/2, nMaskHeight/2);
   
   if (bInvertMask)
      nBackground = nBlack;
      nMask = nWhite;
   else
      nBackground = nWhite;
      nMask = nBlack;
   end
   
   mnPartialMask = double(mbMask);
   mnPartialMask(mbMask) = nMask;
   mnPartialMask(~mbMask) = nBackground;
   
   vnMaskRect = CenterRect([0 0 size(mbMask)], vnScreenRect);
   vnMaskRect = vnMaskRect + round([vfMaskOffsetPix vfMaskOffsetPix]);
   
   tnMask = nGrey * ones(vnScreenRect(4), vnScreenRect(3), 2);
   tnMask(:, :, 2) = nBackground;
   tnMask(vnMaskRect(2)+1:vnMaskRect(4), vnMaskRect(1)+1:vnMaskRect(3), 2) = mnPartialMask;
   
   % - Remove an existing mask texture
   if (exist('PFIS_nMaskTexID', 'var') && ~isempty(PFIS_nMaskTexID) && ismember(PFIS_nMaskTexID, Screen('Windows')))
      try %#ok<TRYNC>
         Screen('Close', PFIS_nMaskTexID);
      end
   end
   
   % - Generate the mask texture
   PFIS_nMaskTexID = Screen('MakeTexture', hWindow, tnMask);
end



% -- Present stimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewFrameRate = [];
    bBlewBlankTime = [];
    return;
end


% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, PFIS_sImageStackCache{nCacheNum}.vnTexIDs);

if (bInvert)
   Screen('PreloadTextures', hWindow, PFIS_sImageStackCache{nCacheNum}.vnInvertTexIDs);
end



% - Find destination rectangle
vnScreenRect = Screen('rect', hWindow);

if (isempty(vfDisplaySizeDeg))
   vnDestRect = [];
else
   vnDestRect = [0 0 vfDisplaySizeDeg(1) vfDisplaySizeDeg(2)] .* [0 0 vfPixelsPerDegree];
   vnDestRect = CenterRect(vnDestRect, vnScreenRect);
   vnDestRect = OffsetRect(vnDestRect, vfCentreOffsetDeg(1) .* vfPixelsPerDegree(1), vfCentreOffsetDeg(2) .* vfPixelsPerDegree(2));
end

% - Work out onset times for each stimulus
nNumStims = numel(vnOrdering);
vtOnsetTimes = ((1:nNumStims)-1) * (vtBlankTime(1) + tFlashDuration) + vtBlankTime(1);
vtRepeatStartTimes = ((1:nNumRepeats)-1) * (vtBlankTime(1) + tFlashDuration) * nNumStims;

if (numel(vtBlankTime) == 2)
   vtRepeatStartTimes = vtRepeatStartTimes + vtBlankTime(2);
else
   vtRepeatStartTimes = vtRepeatStartTimes + tStartTime;
end

% - Start from first stimulus
bBlewFrameRate = false;
tCurrTime = -inf;
tGlobalEndTime = vtRepeatStartTimes(end) + nNumStims * (tFlashDuration + vtBlankTime(1));
   
for (nRepeat = 1:nNumRepeats) %#ok<FORPF>
   % - Reset stimulus params
   bFirstThisRepeat = true;
   vtTheseOnsetTimes = vtOnsetTimes + vtRepeatStartTimes(nRepeat);
   tEndTimeThisRepeat = vtRepeatStartTimes(nRepeat) + nNumStims * (tFlashDuration + vtBlankTime(1));
   
   for (nStimID = 1:nNumStims)
      nStim = vnOrdering(nStimID);
      
      % - Draw texture
      Screen('DrawTexture', hWindow, PFIS_sImageStackCache{nCacheNum}.vnTexIDs(nStim), [], vnDestRect, 0, 1);

      %  - Draw the mask
      if (bDrawMask)
         Screen('Blendfunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
         Screen('DrawTexture', hWindow, PFIS_nMaskTexID);
      end
      
      % - Flip window
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, false, vtTheseOnsetTimes(nStimID));
      
      if (bFirstThisRepeat)
         bBlewBlankTime = Missed>0;
         bFirstThisRepeat = false;
      else
         bBlewBlankTime = bBlewBlankTime | Missed>0;
      end
      
      % - Have we exceeded our deadline?
      if (tCurrTime > tEndTimeThisRepeat)
         break;
      end
      
      if (bInvert)
         % - Present inverted image after required duration
         Screen('DrawTexture', hWindow, PFIS_sImageStackCache{nCacheNum}.vnInvertTexIDs(nStim), [], vnDestRect, 0, 1);

         %  - Draw the mask
         if (bDrawMask)
            Screen('Blendfunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            Screen('DrawTexture', hWindow, PFIS_nMaskTexID);
         end
         
         % - Flip window
         [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, false, vtTheseOnsetTimes(nStimID) + tFlashDuration/2);

         bBlewFrameRate = bBlewFrameRate | Missed>0;
      end
      
      % - Blank screen after required duration
      if (vtBlankTime(1) > 0)
         Screen('FillRect', hWindow, nGrey);
         [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, true, vtTheseOnsetTimes(nStimID) + tFlashDuration);
         bBlewFrameRate = bBlewFrameRate | Missed>0;
      end

      % - Have we exceeded our deadline?
      if (tCurrTime > tEndTimeThisRepeat)
         break;
      end
   end
   
   % - Have we exceeded our deadline?
   if (tCurrTime > tGlobalEndTime)
      break;
   end
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTimeThisRepeat);

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   disp('--- PresentFlashedImageSequence: The desired blanking period or display time was not met.');
end

% --- END of PresentFlashedImageSequence.m ---

