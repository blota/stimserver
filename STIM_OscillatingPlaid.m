function [sStimulus] = STIM_OscillatingPlaid(varargin)

% STIM_OscillatingPlaid - Stimulus object
%
% Usage: [sStimulus] = STIM_OscillatingPlaid( tFrameDuration, tStimulusDuration, ...
%                                              vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
%                                              fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 13th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, tStimulusDuration, ...
   vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
	fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimulusDuration};
cStimParams = {vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
               fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentOscillatingPlaidStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeOscillatingPlaidStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_OscillatingPlaid.m ---
