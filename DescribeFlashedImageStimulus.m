function [strDescription] = DescribeFlashedImageStimulus(sStimulus, strIndent)

% DescribeFlashedImageStimulus - STIMULUS A series of flashed images, with an optional mask
%
% A series of images will be presented in order, with an optional blank period
% between each image.  The images can also be presented with a contrast
% invert, so that the contrast is flipped halfway through the presentation
% of each image.  The order of images in the sequence can be supplied
% dynamically, so the order of presentation can be randomised or a subset
% of images can be selected.
%
% The contrast of each image must be pre-normalised between 0 and 1, but the
% contrast of the images as presented can be set as a group.
%
%
% Stimulus parameters:
%
% tFlashDuration - The duration for which each image should be presented,
%     in seconds.
%
% nNumRepeats - The number of times the full sequence should be presented.
%
% tfImageStack - A matlab tensor [X x Y x N], where each image is [X x Y]
%  pixels, and there are N images in total.  Each image will be normalised
%  to span the full contrast range.
%
% bInvert - If 'true', images will be presented first as provided in
%  'tfImageStack' for half the presentation duration, then contrast-inverted
%  and presented for the remainder of the presentation duration.  No blank
%  will be inserted between the two halves.
%
% vfDisplaySizeDeg - The desired display dimensions of the images, in
%  degrees.
%
% vfCentreOffsetDeg - The offset of the centre of each image from the
%  centre of the display window, in degrees.  Default: [0 0].
%
% vfPixelsPerDegree - The calibration of the display device, in pixels per
%  degree.  A scalar or vector [X Y] can be provided, to define the
%  calibration in the X and Y dimensions independently.
%
% bDrawMask - If 'true', draw a circular mask over each image.  Default:
%  false (no mask).
%
% bInvertMask - If 'false', each image will be presented within a circular
%  mask (default).  If 'true', each image will be presented surrounding a
%  circular blank region.
%
% fMaskDiameterDeg - The diameter of the circular mask, in degrees.
%
% vfMaskOffsetPix - The offset of the centre of the mask from the centre of
%  the display window, in pixels.
%
% fContrast - The desired contrast of each image (Default: 1).  Values 0..1
%  will result in reduced contrast.  Values > 1 will result in clipped
%  intensities.
%
% vnOrdering - A vector of indices, defining which images in 'tfImageStack'
%  to present and in which order.

% DescribeFlashedImageStimulus - FUNCTION Describe the parameters for a series of flashed images
%
% Usage: [strDescription] = DescribeFlashedImageStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir
% Created: 7th January, 2012

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeFlashedImageStimulus: Incorrect usage\n');
   help DescribeFlashedImageStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFlashDuration, nNumRepeats, tfImageStack, bInvert] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});

[vfDisplaySizeDeg, vfCentreOffsetDeg, vfPixelsPerDegree, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
   fContrast, vnOrdering] = ...
   DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFlashDuration = CheckNanSprintf('%.2f ms', [], tFlashDuration, tFlashDuration*1e3);
strNumRepeats = CheckEmptyNanSprintf('%d', '1', [], nNumRepeats, nNumRepeats);
strInvert = CheckEmptyNanSprintf('%d', '(do not invert)', [], bInvert, bInvert);
strDisplaySizeDeg = CheckEmptyNanSprintf('[%d x %d] deg', '(fit screen)', vfDisplaySizeDeg, vfDisplaySizeDeg);
vfCentreOffsetDeg = CheckEmptyNanSprintf('[%.2f, %.2f] deg', '(centred)', vfCentreOffsetDeg, vfCentreOffsetDeg);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], vfPixelsPerDegree, vfPixelsPerDegree);
strDrawMask = CheckEmptyNanSprintf('%d', '(no mask)', [], bDrawMask, bDrawMask);
strInvertMask = CheckEmptyNanSprintf('%d', '(reveal centre)', [], bInvertMask, bInvertMask);
strMaskDiameterDeg = CheckNanSprintf('%.2f deg', [], fMaskDiameterDeg, fMaskDiameterDeg);
strMaskPosPix = CheckNanSprintf('[%d, %d] pix', [], vfMaskPosPix, vfMaskPosPix);
strContrast = CheckEmptyNanSprintf('%d %%', '(full contrast)', [], fContrast, round(fContrast*100));



% -- Produce description
   
strDescription = [strIndent sprintf('Masked movie stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Image flash duration: %s\n', strFlashDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Flash invert each image: %s\n', strInvert) ...
   strIndent sprintf('Image display size: %s\n', strDisplaySizeDeg) ...
   strIndent sprintf('Image centre location offset: %s\n', vfCentreOffsetDeg) ...
   strIndent sprintf('Mask each image: %s\n', strDrawMask) ...
   strIndent sprintf('Invert mask: %s\n', strInvertMask) ...
   strIndent sprintf('Mask diameter: %s\n', strMaskDiameterDeg) ...
   strIndent sprintf('Mask centre position: %s\n', strMaskPosPix) ...
   strIndent sprintf('Image contrast: %s\n', strContrast)];

% --- END of DescribeFlashedImageStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeFlashedImageStimulus.m ---
