function [strDescription] = DescribeGaborGridStimulus(sStimulus, strIndent)

% GaborGridStimulus - STIMULUS An ordered grid of arbitrary Gabors
%
% This stimulus generates a grid of Gabor patches, with independent parameters.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired frame duration, in seconds.  Use
%        'GetSafeFrameDurations' to get a value for this parameter.
%
%     tStimulusDuration - The desired duration of the entire stimulus, in
%        seconds.
%
% Stimulus parameters: 
%tFrameDuration, tStimDuration, 
%     vnGridDims - A vector [nGridWidth nGridHeight], defining the dimensions of
%        the desired Gabor grid.
%
%     fGridSpacingDeg - The spacing between Gabors in the grid, in visual degrees.
%
%     fPixelsPerDeg - The calibration of the stimulus presentation screen, in
%        pixels per degree.
%
%     mfGaborWidthDeg - The width of each Gabor patch, in degrees.  The width is
%        taken as two standard deviations of the Gaussian used to mask the
%        Gabor.  Either a scalar value can be supplied, or a matrix of widths,
%        one for each Gabor patch.
%
%     mfGaborSFCPD - The spatial frequency of each Gabor patch, in cycles per
%        degree.  Either a scalar value can be supplied, or a matrix of spatial
%        frequencies, one for each Gabor patch.
%
%     mfGaborContrast - The contrast of each Gabor patch.  '1' (default) scales
%        the underlying grating such that it spans minimum to maximum luminance
%        on the stimulus screen.  Values greater than one will clip; values less
%        than one will have reduced contrast for each patch.  Either a scalar
%        value can be supplied, or a matrix of contrasts, one for each Gabor
%        patch.
%
%     mfGaborOrientationRad - The orientation (and drift direction) of each
%        Gabor patch, in radians. '0' (default) leads to vertical bars, drifting
%        to the left.  'pi/2' leads to horizontal bars, drifting upwards.
%        Either a scalar value can be supplied, or a matrix of orientations, one
%        for each Gabor patch.
%
%     mfGaborPhaseRad - The initial phase shift of each Gabor patch, in radians.
%        '0' (default) leads to an odd-phase Gabor patch.  Increasing phases
%        shift the underlying grating in the direction of drift (see above).
%        Either a scalar value can be supplied, or a matrix of phases, one
%        for each Gabor patch.
%
%     mfGaborDriftTFHz - The phase drift rate of each Gabor patch, in cycles per
%        second.  By default, Gabors do not drift ('0'). Either a scalar value
%        can be supplied, or a matrix of drift rates, one for each Gabor patch.
%
%     mfGaborRotationHz - The rotation rate of each Gabor patch, in cycles per
%        second.  By default, Gabors do not rotate ('0'). Either a scalar value
%        can be supplied, or a matrix of rotation rates, one for each Gabor
%        patch.

% DescribeGaborGridStimulus - FUNCTION Describe the parameters for a "GaborField" stimulus
%
% Usage: [strDescription] = DescribeGaborGridStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 9th November, 2012

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeGaborGridStimulus: Incorrect usage\n');
   disp('Usage: [strDescription] = DescribeGaborGridStimulus(sStimulus <, strIndent>);');
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
   mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
   mfGaborOrientationRad, mfGaborPhaseRad, ...
   mfGaborDriftTFHz, mfGaborRotationHz] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});
   
strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);

strGridDims = CheckNanSprintf('[%d %d]', [], vnGridDims, vnGridDims);
strGridSpacingDeg = CheckNanSprintf('%.2f deg', [], fGridSpacingDeg, fGridSpacingDeg);
strPixelsPerDeg = CheckNanSprintf('%.2f PPD', [], fPixelsPerDeg, fPixelsPerDeg);
strGaborWidthDeg = CheckNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborWidthDeg)) '] deg'], [], mfGaborWidthDeg, mfGaborWidthDeg);
strGaborSFCPD = CheckNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborSFCPD)) '] CPD'], [], mfGaborSFCPD, mfGaborSFCPD);
strGaborContrast = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborContrast)) ']'], '(1)', [], mfGaborContrast, mfGaborContrast);
strGaborOrientationRad = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborOrientationRad)) '] deg'], '(vertical)', [], mfGaborOrientationRad*180/pi, mfGaborOrientationRad*180/pi);
strGaborPhaseRad = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborPhaseRad)) '] rad'], '(odd)', [], mfGaborPhaseRad, mfGaborPhaseRad);
strGaborDriftTFHz = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborDriftTFHz)) '] Hz'], '(stationary)', [], mfGaborDriftTFHz, mfGaborDriftTFHz);
strGaborRotationHz = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(mfGaborRotationHz)) '] Hz'], '(stationary)', [], mfGaborRotationHz, mfGaborRotationHz);

   
% -- Produce description
   
strDescription = [strIndent sprintf('Regular grid of Gabor patches\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Grid dimensions: %s\n', strGridDims) ...
   strIndent sprintf('Grid element spacing: %s\n', strGridSpacingDeg) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDeg) ...
   strIndent sprintf('Gabor widths: %s\n', strGaborWidthDeg) ...
   strIndent sprintf('Gabor spatial freq.s: %s\n', strGaborSFCPD) ...
   strIndent sprintf('Gabor contrasts: %s\n', strGaborContrast) ...
   strIndent sprintf('Gabor orientations: %s\n', strGaborOrientationRad) ...
   strIndent sprintf('Gabor phases: %s\n', strGaborPhaseRad) ...
   strIndent sprintf('Gabor drit rates: %s\n', strGaborDriftTFHz) ...
   strIndent sprintf('Gabor rotation rates: %s\n', strGaborRotationHz)];

% --- END of DescribeGaborGridStimulus FUNCTION ---


function strString = CheckNanSprintf(strFormat, strNanFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strNanFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeGaborGridStimulus.m ---
