function [strDescription] = DescribeOscillatingGratingStimulus(sStimulus, strIndent)

% OscillatingGratingStimulus - STIMULUS A stationary square-wave grating, that oscillates in contrast
%
% This stimulus is a grey field, with a set of bars overlaid.  These bars
% oscillate between white and black, while remaining stationary.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%        degree.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fAngleDegrees - The orientation of the grating in degrees, where 0 degrees
%        is a vertical grating.
%     fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%        If not provided, then the grating will have a 50% duty cycle.
%     bOscillateOutOfPhase - If set to 'true', the bars will oscillate with
%        every second frame shifted 180 degrees in spatial phase.  This means
%        that every second frame will have bars in "opposite" locations.
%     vfStimulusColours - A three-element vector with the format
%        [fBackground fBar1 fBar2], where the three parameters define the colour
%        of the background field and the colours used for the two sets of bars
%        (frame 1 and frame 2) respectively.  By default, [Grey Black White].
%     bDrawMask - A boolean that determines whether a mask should be drawn
%        over the grating stimulus.  Default: false (no mask).
%     bInvertMask - A boolean that determines whether the grating should be
%        shown within a circular mask (false; default) or whether the
%        grating should be shown surrounding a central blank region (true).
%     fMaskDiameterDeg - The desired width of the circula mask, in degrees.
%        No default; required if a mask must be shown.
%     vfMaskOffsetPix - The offset of the centre of the mask from the
%        centre of the display window, in pixels.  Default: [0 0].
                                               
% DescribeOscillatingGratingStimulus - FUNCTION Describe the parameters for an oscillating grating stimulus
%
% Usage: [strDescription] = DescribeOscillatingGratingStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeOscillatingGratingStimulus: Incorrect usage');
   help DescribeOscillatingGratingStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngleDegrees, ...
   fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix] = ...
   DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfSizeDegrees, vfSizeDegrees);
strCyclesPerDegree = CheckNanSprintf('%.2f cpd; %.2f dpc', [], fCyclesPerDegree, fCyclesPerDegree, 1/fCyclesPerDegree);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strAngleDegrees = CheckNanSprintf('%.2f deg', [], fAngleDegrees, fAngleDegrees);
strBarWidthDegrees = CheckEmptyNanSprintf('%.2f deg', '(0.5 duty cycle)', [], fBarWidthDegrees, fBarWidthDegrees);
strOscillateOutOfPhase = CheckEmptyNanSprintf('%d', '(off)', '(dynamic or off)', bOscillateOutOfPhase, bOscillateOutOfPhase);
strStimulusColours = ['[' CheckEmptyNanSprintf(' %d', '(0.5 0 1)', '(dynamic or 0.5 0 1)', vfStimulusColours, vfStimulusColours) ']']';
strDrawMask = CheckEmptyNanSprintf('%d', '(no mask)', [], bDrawMask, bDrawMask);
strInvertMask = CheckEmptyNanSprintf('%d', '(reveal centre)', [], bInvertMask, bInvertMask);
fMaskDiameterDeg = CheckNanSprintf('%.2f deg', [], fMaskDiameterDeg, fMaskDiameterDeg);
strMaskPosPix = CheckNanSprintf('[%d, %d] pix', [], vfMaskOffsetPix, vfMaskOffsetPix);


% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating grating stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Grating size: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Grating angle: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating bar width: %s\n', strBarWidthDegrees) ...
   strIndent sprintf('Oscillate out of phase: %s\n', strOscillateOutOfPhase) ...
   strIndent sprintf('Stimulus colours: %s\n', strStimulusColours) ...
   strIndent sprintf('Mask stimulus: %s\n', strDrawMask) ...
   strIndent sprintf('Inverted mask: %s\n', strInvertMask) ...
   strIndent sprintf('Mask diameter: %s\n', fMaskDiameterDeg) ...
   strIndent sprintf('Mask centre position: %s\n', strMaskPosPix)];


% --- END of DescribeOscillatingGratingStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeOscilatingGratingStimulus.m ---
