function SStalkback(varargin)

% SStalkback - Send a message to the talkback interface
%
% Usage: SStalkback(...)
%
% Usage semantics are as for fprintf, sprintf, ...

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

% -- Global state

global STIMSERV_sState;


% -- Echo text to talkback interface, if possible

try
   % - Append a terminator to the talkback string
   varargin{1} = [varargin{1} STIMSERV_sState.vnTalkbackTerminator];
   
   if (STIMSERV_sState.bUsePNET)
      % - Is there actually an open (TCP) connection?
      if (STIMSERV_sState.bUseTCP && pnet(STIMSERV_sState.hTalkbackSocket, 'status') ~= 11)
         % - No, so try to close the port
         if (pnet(STIMSERV_sState.hTalkbackSocket, 'status') == 0)
            pnet(STIMSERV_sState.hTalkbackSocket, 'close');
         end
         
         % - And try to open a new connection
         % - Make a TCP connection
         STIMSERV_sState.hTalkbackSocket = pnet('tcpconnect', STIMSERV_sState.oTalkback{1}, STIMSERV_sState.oTalkback{2});
      end
      
      % - Send string using PNET
      pnet(STIMSERV_sState.hTalkbackSocket, 'printf', varargin{:});
   
      if (~STIMSERV_sState.bUseTCP)
         % - Transmit the write buffer
         pnet(STIMSERV_sState.hTalkbackSocket, 'writepacket');
      end
      
   else
      % - Send string using instrument control toolbox
      fprintf(STIMSERV_sState.hTalkbackSocket, varargin{:});
   end
   
catch errMsg
   % - Just fail quietly
end

% --- END of SStalkback.m ---
