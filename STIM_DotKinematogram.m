function [sStimulus] = STIM_DotKinematogram(varargin)

% STIM_DotKinematogram - Stimulus object
%
% Usage: [sStimulus] = STIM_DotKinematogram( tFrameDuration, tStimDuration, ...
%                                            fDotDiameterDeg, fPixelsPerDegree, fDotDensityPerDegreeSqr, ...
%                                            fDriftRateDegPerSec, tMaxPersistenceSec, ...
%                                            fCoherence, fCoherentDirectionRad, ...
%                                            vnFieldColour, vnDotColour, nSeed)
% 
% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 2nd November, 2012

%- Deal out parameters from argument list
[tFrameDuration, tStimDuration, fDotDiameterDeg, fPixelsPerDegree, ...
   fDotDensityPerDegreeSqr, fDriftRateDegPerSec, tMaxPersistenceSec, ...
   fCoherence, fCoherentDirectionRad, vnFieldColour, vnDotColour, nSeed] = ...
      DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimDuration};
cStimParams = {fDotDiameterDeg, fPixelsPerDegree, ...
               fDotDensityPerDegreeSqr, fDriftRateDegPerSec, tMaxPersistenceSec, ...
               fCoherence, fCoherentDirectionRad, vnFieldColour, vnDotColour, nSeed};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentDotKinematogram, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeDotKinematogram);
           

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_DotKInematogram.m ---
