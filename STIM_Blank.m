function [sStimulus] = STIM_Blank(varargin)

% STIM_Blank - Stimulus object
%
% Usage: [sStimulus] = STIM_Blank(tStimBlankTime)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 26th October, 2010

%- Deal out parameters from argument list
[tStimBlankTime] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tStimBlankTime};
cStimParams = {};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Make "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{1});

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentBlank, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeBlankStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_Blank.m ---
