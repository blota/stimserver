function [sStimulus] = STIM_OscillatingGrating(varargin)

% STIM_OscillatingGrating - Stimulus object
%
% Usage: [sStimulus] = STIM_OscillatingGrating( tFrameDuration, tStimulusDuration, ...
%                                               vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngleDegrees, ...
%                                               fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
%                                               bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 12th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, tStimulusDuration, ...
   vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngleDegrees, ...
   fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimulusDuration};
cStimParams = {vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngleDegrees, ...
               fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
               bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentOscillatingGratingStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeOscillatingGratingStimulus);


% --- END of STIM_OscillatingGrating.m ---


function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));

