function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
   PresentDotKinematogram( hWindow, vtBlankTime, ...
   tFrameDuration, tStimDuration, fDotDiameterDeg, fPixelsPerDegree, fDotDensityPerDegreeSqr, ...
   fDriftRateDegPerSec, tMaxPersistenceSec, fCoherence, fCoherentDirectionRad, vnFieldColour, vnDotColour, nSeed)

% PresentDotKinematogram - FUNCTION Present a random dot kinematogram stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%           PresentDotKinematogram( hWindow, vtBlankTime, ...
%              tFrameDuration, tStimDuration, fDotDiameterDeg, fPixelsPerDegree, fDotDensityPerDegreeSqr, ...
%              fDriftRateDegPerSec, tMaxPersistenceSec, fCoherence, fCoherentDirectionRad, ...
%              vnFieldColour, vnDotColour, nSeed)

% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 2nd November, 2012

% -- Check arguments

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end

if (~exist('nSeed', 'var') || isempty(nSeed))
   nSeed = 0;
end

if bPresentStimulus && (nargin < 11)
   disp('*** PresentDotKinematogram: Incorrect usage');
   help PresentDotKinematogram;
   return;
end

%% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewBlankTime = [];
   bBlewFrameRate = [];
   return;
end


% -- Generate initial dot locations and movement directions

% - Seed RNG
if (~isempty(nSeed))
   rand('twister', nSeed); %#ok<RAND>
end

[vfWindowSizeDeg(1), vfWindowSizeDeg(2)] = Screen('WindowSize', hWindow);
vfWindowSizeDeg = vfWindowSizeDeg ./ fPixelsPerDegree;
nNumDots = floor(vfWindowSizeDeg(1) * vfWindowSizeDeg(2) * fDotDensityPerDegreeSqr);

vbMissingDots = true(nNumDots, 1);
mfDotLocationsDeg = nan(2, nNumDots);
vfDotDirectionsDeg = nan(1, nNumDots);
vtDotStartTime = nan(1, nNumDots);
nNumDraws = 0;
while (any(vbMissingDots) && nNumDraws < 10)
   [mfDotLocationsDeg, vfDotDirectionsDeg, vbMissingDots] = ReplaceDots(mfDotLocationsDeg, vfDotDirectionsDeg, ...
                                                               vfWindowSizeDeg, fDotDiameterDeg, fCoherence, fCoherentDirectionRad, vbMissingDots);
	nNumDraws = nNumDraws + 1;
end

% - We are now guaranteed to have a set of non-overlapping dots


% -- Determine nWhite and nBlack

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = (nWhite+nBlack)/2;

if (~exist('vnFieldColour', 'var') || isempty(vnFieldColour))
   vnFieldColour = nBlack;
end

if (~exist('vnDotColour', 'var') || isempty(vnDotColour))
   vnDotColour = nWhite;
end


% - Start from first stimulus frame
bFirst = true;
bBlewFrameRate = false;
tNextFlipTime = tFlipTime;
tLastFlipTime = -inf;
tCurrFrameDur = nan;
tCurrTime = -inf;
tEndTime = inf;

while (tNextFlipTime <= tEndTime)
   % - Draw the set of dots on the screen
   Screen('BlendFunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   Screen('FillRect', hWindow, vnFieldColour);
   Screen('DrawDots', hWindow, mfDotLocationsDeg .* fPixelsPerDegree, fDotDiameterDeg .* fPixelsPerDegree, vnDotColour, [0 0], 1);
   
   % - Flip window
   [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tNextFlipTime); %#ok<ASGLU>
   
   % - Initialise stimulation and check deadlines   
   if (bFirst)
      bBlewBlankTime = Missed > 0;
      tStartTime = tCurrTime;
      tNextFlipTime = tCurrTime;
      tLastFlipTime = tCurrTime - tFrameDuration;
      tCurrFrameDur = tFrameDuration;
      tEndTime = tStartTime + tStimDuration;
      vtDotStartTime(:) = tStartTime + (rand(1, nNumDots) - 0.5) * tMaxPersistenceSec;
      bFirst = false;
   else
      bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      tCurrFrameDur = tCurrTime - tLastFlipTime;
   end
   
   % - Determine next flip time
   tNextFlipTime = tNextFlipTime + tFrameDuration;
   tLastFlipTime = tCurrTime;

   % -- Shift dots and determine which need to be replaced
   
   % - Which dots have expired?
   vbExpired = (tCurrTime - vtDotStartTime(~vbMissingDots)) > tMaxPersistenceSec;
   vbMissingDots(~vbMissingDots) = vbExpired;
   
   % - Shift dots by one frame's worth
   mfDotLocationsDeg(:, ~vbMissingDots) = mfDotLocationsDeg(:, ~vbMissingDots) + fDriftRateDegPerSec * tCurrFrameDur * [cos(vfDotDirectionsDeg(~vbMissingDots)); sin(vfDotDirectionsDeg(~vbMissingDots))];
   
   % - Which dots are now off screen?
   vbOffScreen = any(bsxfun(@lt, mfDotLocationsDeg(:, ~vbMissingDots), fDotDiameterDeg/2 * [1 1]')) | any(bsxfun(@gt, mfDotLocationsDeg(:, ~vbMissingDots), vfWindowSizeDeg' - (fDotDiameterDeg/2)));
   vbMissingDots(~vbMissingDots) = vbOffScreen;
   
   % - Which of the remaining dots now overlap each other?
   vbOverlap = any(triu(squareform(pdist(mfDotLocationsDeg(:, ~vbMissingDots)', 'cityblock')) - fDotDiameterDeg * sqrt(2), 1) < 0);
   vbMissingDots(~vbMissingDots) = vbOverlap;
   
   % -- Draw new random dot locations (only once) and reset appearance times
   [mfDotLocationsDeg, vfDotDirectionsDeg, vbStillMissing] = ReplaceDots(mfDotLocationsDeg, vfDotDirectionsDeg, ...
                                                               vfWindowSizeDeg, fDotDiameterDeg, fCoherence, fCoherentDirectionRad, vbMissingDots);
	vtDotStartTime(vbMissingDots & ~vbStillMissing) = tCurrTime;
   vbMissingDots = vbStillMissing;
end

% - Display a warning
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentSimpleStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Blank screen and return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);


end

% --- END of PresentDotKinematogram FUNCTION ---


function [mfDotLocationsDeg, vfDotDirectionsDeg, vbStillReplace] = ReplaceDots( mfDotLocationsDeg, vfDotDirectionsDeg, ...
   vfMaxSizeDeg, fDotDiameterDeg, fCoherence, fCoherentDirectionRad, vbReplaceDots)

   % - Do any dots need to be replaced?
   if (any(vbReplaceDots))
      % - Find valid dimensions for placing dots (ignore borders)
      vfWindowWidth = vfMaxSizeDeg - fDotDiameterDeg;
      vfWindowMin = fDotDiameterDeg/2 * [1 1];
      
      % - Remove dots that should be replaced
      mfDotLocationsDeg(:, vbReplaceDots) = nan;
      vfDotDirectionsDeg(vbReplaceDots) = nan;
      
      % - Find new random locations for the dots that should be replaced
      mfNewDotLocs = bsxfun(@plus, bsxfun(@times, rand(2, nnz(vbReplaceDots)), vfWindowWidth'), vfWindowMin');
      
      % - Compute manhattan-distance overlap test with existing dots
      mfDistX = abs(bsxfun(@minus, mfNewDotLocs(1, :), mfDotLocationsDeg(1, :)'));
      mfDistY = abs(bsxfun(@minus, mfNewDotLocs(2, :), mfDotLocationsDeg(2, :)'));
      mfDist = mfDistX + mfDistY;
      vbOverlap = any(mfDist < fDotDiameterDeg * sqrt(2));
      
      % - Compute manhattan-distance overlap test within new dots
      vbNewOverlap = any(triu(squareform(pdist(mfNewDotLocs(:, ~vbOverlap)', 'cityblock')) - fDotDiameterDeg * sqrt(2), 1) < 0);
      vbOverlap(~vbOverlap) = vbNewOverlap;
      
      % - Insert dots that don't overlap with existing dots
      vbStillReplace = vbReplaceDots;
      vbStillReplace(vbReplaceDots) = vbOverlap;
      vbReplaceDots(vbReplaceDots) = ~vbOverlap;
      mfDotLocationsDeg(:, vbReplaceDots) = mfNewDotLocs(:, ~vbOverlap);
      
      % - Draw random directions for the new dots
      % - Make sure the coherence parameter is respected as closely as possible
      nTotalDots = nnz(~vbStillReplace);
      nCoherentDesired = round(nTotalDots * fCoherence);
      nCurrentCoherent = nnz(vfDotDirectionsDeg == fCoherentDirectionRad);
      nNumNewCoherent = nCoherentDesired - nCurrentCoherent;
      
      % - Assign coherent dots
      if (nNumNewCoherent > 0)
         vbCoherentDots = false(size(vbReplaceDots));
         vbCoherentDots(find(vbReplaceDots, nNumNewCoherent, 'first')) = true;
         vbReplaceDots = vbReplaceDots & ~vbCoherentDots;
         
         vfDotDirectionsDeg(vbCoherentDots) = fCoherentDirectionRad;
      end
      
      % - Assign random dots
      vfDotDirectionsDeg(vbReplaceDots) = rand(1, nnz(vbReplaceDots)) * 2*pi;
   else
      vbStillReplace = vbReplaceDots;
   end
end

% --- END of PresentDotKinematogram.m ---
