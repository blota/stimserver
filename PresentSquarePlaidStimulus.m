function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSquarePlaidStimulus(   hWindow, vtBlankTime, ...
                                        tFrameDuration, tStimulusDuration, nNumRepeats, ...
                                        vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
                                        vfAngleDegrees, vfShiftCyclesPerSec, vfRotateCyclesPerSec, ...
                                        vfContrast)
                             
% PresentSquarePlaidStimulus - FUNCTION Present a square wave plaid stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             SquarePlaidStimulus(  hWindow, vtBlankTime, ...
%                                        tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                        vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
%                                        vfAngleDegrees, <vfShiftCyclesPerSec, vfRotateCyclesPerSec>, ...
%                                        <vfContrast>)
%
% 'vfSizeDegrees' is a vector [X Y] that defines the size of the entire
% stimulus, in degrees.  Leave as an empty vector to fill the presentation
% screen.

% Author: Dylan Muir
% Created: 9th January, 2012


% -- Persistent arguments

persistent PSPS_CACHE PSPS_sTextureCache;


% -- Blank screen

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end

% -- Check arguments

if (nargin < 9)
   disp('*** SquarePlaidStimulus: Incorrect usage');
   help SquarePlaidStimulus;
   return;
end

if (isscalar(vfAngleDegrees))
   vfAngleDegrees = [vfAngleDegrees vfAngleDegrees];
end

if (isscalar(vfCyclesPerDegree))
   vfCyclesPerDegree = [vfCyclesPerDegree vfCyclesPerDegree];
end

if (~exist('vfShiftCyclesPerSec', 'var') || isempty(vfShiftCyclesPerSec))
    vfShiftCyclesPerSec = [0 0];
end

if (isscalar(vfShiftCyclesPerSec))
   vfShiftCyclesPerSec = [vfShiftCyclesPerSec vfShiftCyclesPerSec];
end

if (~exist('vfRotateCyclesPerSec', 'var') || isempty(vfRotateCyclesPerSec))
    vfRotateCyclesPerSec = [0 0];
end

if (isscalar(vfRotateCyclesPerSec))
   vfRotateCyclesPerSec = [vfRotateCyclesPerSec vfRotateCyclesPerSec];
end

if (~exist('vfContrast', 'var') || isempty(vfContrast))
   vfContrast = [.5 .5];
end

if (isscalar(vfContrast))
   vfContrast = [vfContrast vfContrast];
end


% - Do we need to re-generate the stimulus?

sStimParams.vfSizeDegrees = vfSizeDegrees;
sStimParams.vfCyclesPerDegree = vfCyclesPerDegree;
sStimParams.fPixelsPerDegree = fPixelsPerDegree;

[nCacheNum, bCacheHit, bCacheCollision, sCachedParams, PSPS_CACHE] = ...
   ParamCache(sStimParams, PSPS_CACHE);

bGenerateStim = ~bCacheHit | bCacheCollision;



% -- Generate stimulus

if (bGenerateStim)
   disp('--- SquarePlaidStimulus: Generating stimulus');
   
   % - Record parameters
   PSPS_sTextureCache{nCacheNum}.vfCyclesPerDegree = vfCyclesPerDegree;
   PSPS_sTextureCache{nCacheNum}.hWindow = hWindow;
   PSPS_sTextureCache{nCacheNum}.fPixelsPerDegree = fPixelsPerDegree;   
   PSPS_sTextureCache{nCacheNum}.vfSizeDegrees = vfSizeDegrees;
   PSPS_sTextureCache{nCacheNum}.vfPixelsPerCycle = fPixelsPerDegree ./ vfCyclesPerDegree;
   PSPS_sTextureCache{nCacheNum}.vfContrast = vfContrast;
   
   % - Determine how big the stimulus should be, so it can be rotated and
   %   shifted
   if (isempty(vfSizeDegrees))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      
      vfScreenSizeDegrees = sqrt(sum((vnScreenSize / fPixelsPerDegree).^2)) .* [1 1];
      vfSizeDegrees = (sqrt(sum(vfScreenSizeDegrees.^2)) + 1/min(vfCyclesPerDegree)) * [1 1];
   end
   
   % - Generate stimulus
   mfGrating1 = GenerateSquareGratingStimulus(vfSizeDegrees, vfCyclesPerDegree(1), fPixelsPerDegree);
   mfGrating2 = GenerateSquareGratingStimulus(vfSizeDegrees, vfCyclesPerDegree(2), fPixelsPerDegree);
   
   % - Convert to [-0.5..0.5]
   mfGrating1 = (mfGrating1 - .5);
   mfGrating2 = (mfGrating2 - .5);
   
   % - Store gratings
   PSPS_sTextureCache{nCacheNum}.mfGrating1 = mfGrating1;
   PSPS_sTextureCache{nCacheNum}.mfGrating2 = mfGrating2;
   
   % - Clear texture cache
   PSPS_sTextureCache{nCacheNum}.vnTextureIDs = [];
end

% - Create textures, if necessary
if (isempty(PSPS_sTextureCache{nCacheNum}.vnTextureIDs))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PSPS_sTextureCache{nCacheNum}.vnTextureIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate a shader to use
   hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');

   % - Generate the textures
   PSPS_sTextureCache{nCacheNum}.vnTextureIDs(1) = Screen('MakeTexture', hWindow, PSPS_sTextureCache{nCacheNum}.mfGrating1, [], [], 2, [], hGlsl);
   PSPS_sTextureCache{nCacheNum}.vnTextureIDs(2) = Screen('MakeTexture', hWindow, PSPS_sTextureCache{nCacheNum}.mfGrating2, [], [], 2, [], hGlsl);
end


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewBlankTime = [];
   bBlewFrameRate = [];
   return;
end

% - Work out presenation parameters
vnTextureRect1 = Screen('Rect', PSPS_sTextureCache{nCacheNum}.vnTextureIDs(1));
vnTextureRect2 = Screen('Rect', PSPS_sTextureCache{nCacheNum}.vnTextureIDs(2));
vnScreenRect = Screen('Rect', hWindow);
vnTextureRestLeft(1) = floor((vnTextureRect1(3) - vnTextureRect1(1) ...
    - ((vnScreenRect(3) - vnScreenRect(1)) ...
    * abs(cos((mod(vfAngleDegrees(1),180))/180*pi)) ...
    + (vnScreenRect(4) - vnScreenRect(2)) ...
    * (sin((mod(vfAngleDegrees(1),180))/180*pi))))/2);

vnTextureRestLeft(2) = floor((vnTextureRect2(3) - vnTextureRect2(1) ...
    - ((vnScreenRect(3) - vnScreenRect(1)) ...
    * abs(cos((mod(vfAngleDegrees(2),180))/180*pi)) ...
    + (vnScreenRect(4) - vnScreenRect(2)) ...
    * (sin((mod(vfAngleDegrees(2),180))/180*pi))))/2);

vnTextureRestLeft = min(vnTextureRestLeft) * [1 1];
 
% -- Fill the screen with grey

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);

if (nGrey == nWhite)
   nGrey = (nWhite + nBlack)/2;
end

Screen('FillRect', hWindow, [nGrey nGrey nGrey 255]);

% - Start from first stimulus frame
bFirst = true;
bBlewFrameRate = false;
tNextFlipTime = tFlipTime;

for (nRepeat = 1:nNumRepeats) %#ok<FORPF>
   
   % - Reset stimulus params
   tCurrTime = -inf;
   tEndTime = inf;
   bFirstThisRepeat = true;
   vfCurrAngle = vfAngleDegrees;
   vfCurrShift = [0 0];
   
   while (tCurrTime <= tEndTime)
      % - Set up blend function for overlapping gratings
      Screen('BlendFunction', hWindow, GL_SRC_ALPHA, GL_ONE);
      
      % - Draw both gratings onto the screen, centred, shifted and rotated
      Screen('DrawTexture', hWindow, PSPS_sTextureCache{nCacheNum}.vnTextureIDs(1), [], [], vfCurrAngle(1), [], vfContrast(1), [], [], [], [0 mod(vfCurrShift(1), PSPS_sTextureCache{nCacheNum}.vfPixelsPerCycle(1))-vnTextureRestLeft(1) 0 0]);
      Screen('DrawTexture', hWindow, PSPS_sTextureCache{nCacheNum}.vnTextureIDs(2), [], [], vfCurrAngle(2), [], vfContrast(2), [], [], [], [0 mod(vfCurrShift(2), PSPS_sTextureCache{nCacheNum}.vfPixelsPerCycle(2))-vnTextureRestLeft(2) 0 0]);
      
      % - Flip window
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirstThisRepeat, tNextFlipTime); %#ok<ASGLU>
      
      % - Initialise stimulation and check deadlines
      if (bFirst)
         bBlewBlankTime = Missed > 0;
         tStartTime = tCurrTime;
         tNextFlipTime = tCurrTime;
         tCurrFrameDuration = tFrameDuration;
         bFirst = false;
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
         tCurrFrameDuration = tCurrTime - tLastFlipTime;
      end
      
      % - Reset end time for this stimulus repetition      
      if (bFirstThisRepeat)
         tEndTime = tStartTime + nRepeat * tStimulusDuration;
         bFirstThisRepeat = false;
      end
      
      % - Determine next flip time
      tNextFlipTime = tNextFlipTime + tFrameDuration;
      tLastFlipTime = tCurrTime;
      
      % - Update shift and rotation parameters
      vfCurrAngle = vfCurrAngle + vfRotateCyclesPerSec .* tCurrFrameDuration * 360;
      vfCurrShift = vfCurrShift + vfShiftCyclesPerSec ./ vfCyclesPerDegree * fPixelsPerDegree * tCurrFrameDuration;
   end
end

% - Display a warning
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentSquarePlaidStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);




% --- END of SquarePlaidStimulus.m ---


