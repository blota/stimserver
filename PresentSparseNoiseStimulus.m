function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSparseNoiseStimulus(hWindow, vtBlankTime, ...
                                       tFrameDuration, nNumRepeats, ...
                                       vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
                                       vnSeed, nNumFrames, vfPixelValues)

% PresentSparseNoiseStimulus - FUNCTION Present a sparse noise stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentSparseNoiseStimulus(hWindow, vtBlankTime, ...
%                                        tFrameDuration, nNumRepeats, ...
%                                        vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
%                                        < vnSeed, nNumFrames, vfPixelValues>)
%
% 'vfUnitSizeDegrees' is a vector [X Y] defining the size of a single stimulus
% unit in degrees.  'vnNumUnits' is a vector [X Y] defining the number of
% stimulus units in the stimulus. 'fPixelsPerDegree' is the display calibration
% number.  'hWindow' is a handle to an open window.  'tBlankTime' is the desired
% time to show a blank screen from the start of the function to the onset of the
% stimulus, in seconds. 'tFrameDuration' is the desired frame duration in
% seconds.  'nNumRepeats' is the desired number of times to display the entire
% stimulus (with no intervening blank). 'vnSeed' are the seeds used to generate
% the random sparse noise stimulus.  If a vector of seeds are provided, a longer
% sequence built up of the individual seeded sequences will be generated.
% 'nNumFrames', if provided, can be used to present a non-maximal noise
% sequence. 'vfPixelValues' defines the values displayed in the sequence -- see
% GenerateSparseNoiseStimulus for details.

% -- Persistent arguments

persistent PSNS_sParams PSNS_tfSparseNoise PSNS_vnTexIDs;


if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



% -- Check arguments

if (~exist('vnSeed', 'var'))
   vnSeed = [];
end

if (~exist('nNumFrames', 'var'))
   nNumFrames = [];
end

if (~exist('vfPixelValues', 'var') || isempty(vfPixelValues))
   % - Get black / white colours
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   nGrey = round((nWhite + nBlack)/2);
   
   % - Default is [grey black white]
   vfPixelValues = [nGrey nBlack nWhite];
end


% - Do we need to re-generate the stimulus?

if (isempty(PSNS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSNS_sParams.vnSeed, vnSeed);
   bGenerateStim = bGenerateStim | ~isequal(PSNS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSNS_sParams.vnNumUnits, vnNumUnits);
   bGenerateStim = bGenerateStim | ~isequal(PSNS_sParams.nNumFrames, nNumFrames);
   bGenerateStim = bGenerateStim | ~isequal(PSNS_sParams.vfPixelValues, vfPixelValues);
end


% -- Generate stimulus as single pixels

if (bGenerateStim)
   disp('--- PresentSparseNoiseStimulus: Generating stimulus');
   
   % - Record parameters
   PSNS_sParams = [];
   PSNS_sParams.vnSeed = vnSeed;
   PSNS_sParams.hWindow = hWindow;
   PSNS_sParams.vnNumUnits = vnNumUnits;
   PSNS_sParams.nNumFrames = nNumFrames;
   PSNS_sParams.vfPixelValues = vfPixelValues;   
   
   % - Generate stimulus
   PSNS_tfSparseNoise = GenerateSparseNoiseStimulus([1 1], vnNumUnits, vnSeed, nNumFrames, vfPixelValues);
   
   % - Pre-calculate params
   PSNS_sParams.nNumFrames = size(PSNS_tfSparseNoise, 3);
   PSNS_sParams.vnScreenRect = [0 0 0 0];
   [PSNS_sParams.vnScreenRect(3), PSNS_sParams.vnScreenRect(4)] = Screen('WindowSize', hWindow);
   PSNS_sParams.vnDestRec = CenterRect([0 0 round(vfUnitSizeDegrees .* vnNumUnits * fPixelsPerDegree)], PSNS_sParams.vnScreenRect);
   
   % - Re-generate textures
   PSNS_vnTexIDs = [];
end


% - Create textures, if necessary
if (isempty(PSNS_vnTexIDs))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PSNS_vnTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate the texture
   PSNS_vnTexIDs = GenerateTextures(permute(PSNS_tfSparseNoise, [2 1 3]), hWindow);
end



% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, PSNS_vnTexIDs);


% -- Present stimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewFrameRate = [];
    bBlewBlankTime = [];
    return;
end


bFirst = true;
bBlewFrameRate = false;
tEndTime = tFlipTime + nNumRepeats * PSNS_sParams.nNumFrames * tFrameDuration;

for (nRepeat = 1:nNumRepeats)
   for (nFrame = 1:PSNS_sParams.nNumFrames)
      % - Draw texture and flip window
      Screen('DrawTexture', hWindow, PSNS_vnTexIDs(nFrame), [], PSNS_sParams.vnDestRec, [], 0);
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime);

      % - Did we miss any deadlines?
      if (bFirst)
         bBlewBlankTime = Missed > 0;
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      end

      bFirst = false;
      
      % - Compute next flip time
      tFlipTime = tCurrTime + tFrameDuration;
      
      % - Have we blown our stimulation slot?
      if (tFlipTime > tEndTime)
         break;
      end
   end
end

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   disp('--- PresentSparseNoiseStimulus: The desired frame rate or blanking period was not met.');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);

% --- END of PresentSparseNoiseStimulus.m ---

