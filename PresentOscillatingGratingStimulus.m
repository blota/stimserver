function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentOscillatingGratingStimulus(  hWindow, vtBlankTime, ...
                                                tFrameDuration, tStimulusDuration, ...
                                                vfSizeDegrees, fCyclesPerDegree, vfPixelsPerDegree, fAngleDegrees, ...
                                                fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
                                                bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix)

% PresentOscillatingGratingStimulus - FUNCTION Present an oscillating square-wave grating stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentOscillatingGratingStimulus(  hWindow, vtBlankTime, ...
%                                                 tFrameDuration, tStimulusDuration, ...
%                                                 vfSizeDegrees, fCyclesPerDegree, vfPixelsPerDegree, fAngleDegrees, ...
%                                                 < fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours, ...
%                                                 bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix >)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010 (from PresentSparseNoiseStimulus.m)


% -- Persistent arguments

persistent POGS_CACHE POGS_sParams;


if (nargin < 8)
   SSlog('*** PresentOscillatingGratingStimulus: Incorrect usage\n');
   help PresentOscillatingGratingStimulus;
   return;
end

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



% -- Check arguments

if (isscalar(vfPixelsPerDegree))
   vfPixelsPerDegree = vfPixelsPerDegree([1 1]);
end

if (~exist('fBarWidthDegrees', 'var'))
   fBarWidthDegrees = [];
end

if (~exist('bOscillateOutOfPhase', 'var'))
   bOscillateOutOfPhase = [];
end

if (~exist('vfStimulusColours', 'var'))
   vfStimulusColours = [];
end

if (~exist('bDrawMask', 'var') || isempty(bDrawMask))
   bDrawMask = false;
end


% - Do we need to re-generate the stimulus?

sStimParams.fCyclesPerDegree = fCyclesPerDegree;
sStimParams.vfPixelsPerDegree = vfPixelsPerDegree;
sStimParams.fBarWidthDegrees = fBarWidthDegrees;
sStimParams.vfSizeDegrees = vfSizeDegrees;
sStimParams.bOscillateOutOfPhase = bOscillateOutOfPhase;
sStimParams.vfStimulusColours = vfStimulusColours;

[nCacheNum, bCacheHit, bCacheCollision, sCachedParams, POGS_CACHE] = ...
   ParamCache(sStimParams, POGS_CACHE);

bGenerateStim = ~bCacheHit | bCacheCollision;


% -- Generate stimulus

% - Get screen size and colour indices
vnScreenRect = Screen('Rect', hWindow');

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);

if (nGrey == nWhite)
   nGrey = (nWhite + nBlack)/2;
end

if (bGenerateStim)
   SSlog('--- PresentOscillatingGratingStimulus: Generating stimulus\n');
   
   % - Record parameters
   POGS_sParams(nCacheNum).fCyclesPerDegree = fCyclesPerDegree;
   POGS_sParams(nCacheNum).hWindow = hWindow;
   POGS_sParams(nCacheNum).vfPixelsPerDegree = vfPixelsPerDegree;
   POGS_sParams(nCacheNum).fBarWidthDegrees = fBarWidthDegrees;   
   POGS_sParams(nCacheNum).vfSizeDegrees = vfSizeDegrees;
   POGS_sParams(nCacheNum).bOscillateOutOfPhase = bOscillateOutOfPhase;
   POGS_sParams(nCacheNum).vfStimulusColours = vfStimulusColours;
   
   if (isempty(vfSizeDegrees))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      
      vfSizeDegrees = sqrt(sum((vnScreenSize / vfPixelsPerDegree).^2)) .* [1 1];
   end
   
   % - Generate stimulus
   [mfGrating1 mfGrating2] = GenerateOscillatingGratingStimulus(vfSizeDegrees, fCyclesPerDegree, vfPixelsPerDegree, ...
                                                                fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours);
   tfGratings = cat(3, mfGrating1, mfGrating2);
   
   % - Convert to screen colour indices
   tfGratings(tfGratings == 0) = nBlack;
   tfGratings(tfGratings == 0.5) = nGrey;
   tfGratings(tfGratings == 1) = nWhite;
   POGS_sParams(nCacheNum).tfGratings = tfGratings;
   
   % - Clear texture buffer
   POGS_sParams(nCacheNum).vnTexIDs = [];
end

% - Create textures, if necessary
if (isempty(POGS_sParams(nCacheNum).vnTexIDs))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(POGS_sParams(nCacheNum).vnTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate the texture
   POGS_sParams(nCacheNum).vnTexIDs = GenerateTextures(POGS_sParams(nCacheNum).tfGratings, hWindow);
end


% -- Generate a mask texture

if (bDrawMask)
   nMaskWidth = fMaskDiameterDeg * vfPixelsPerDegree(1);
   nMaskHeight =  fMaskDiameterDeg * vfPixelsPerDegree(2);
   mbMask = Ellipse(nMaskWidth/2, nMaskHeight/2);
   
   if (bInvertMask)
      nBackground = nBlack;
      nMask = nWhite;
   else
      nBackground = nWhite;
      nMask = nBlack;
   end
   
   mnPartialMask = double(mbMask);
   mnPartialMask(mbMask) = nMask;
   mnPartialMask(~mbMask) = nBackground;
   
   vnMaskRect = CenterRect([0 0 size(mbMask)], vnScreenRect);
   vnMaskRect = vnMaskRect + round([vfMaskOffsetPix vfMaskOffsetPix]);
   
   tnMask = nGrey * ones(vnScreenRect(4), vnScreenRect(3), 2);
   tnMask(:, :, 2) = nBackground;
   tnMask(vnMaskRect(2)+1:vnMaskRect(4), vnMaskRect(1)+1:vnMaskRect(3), 2) = mnPartialMask;
   
   % - Remove an existing mask texture
   if (exist('POGS_nMaskTexID', 'var') && ~isempty(POGS_nMaskTexID) && ismember(POGS_nMaskTexID, Screen('Windows')))
      try %#ok<TRYNC>
         Screen('Close', POGS_nMaskTexID);
      end
   end
   
   % - Generate the mask texture
   POGS_nMaskTexID = Screen('MakeTexture', hWindow, tnMask);
end


% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, POGS_sParams(nCacheNum).vnTexIDs);


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewFrameRate = [];
   bBlewBlankTime = [];
   return;
end

bFirst = true;
nShowFrame = 1;
bBlewFrameRate = false;
tEndTime = inf;

while (tFlipTime < tEndTime)
   % - Draw texture and flip window
   Screen('BlendFunction', hWindow, GL_ONE, GL_ZERO);
   Screen('DrawTexture', hWindow, POGS_sParams(nCacheNum).vnTexIDs(nShowFrame), [], [], fAngleDegrees);
   
   %  - Draw the mask
   if (bDrawMask)
      Screen('Blendfunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      Screen('DrawTexture', hWindow, POGS_nMaskTexID);
   end
   
   % - Flip window
   [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime); %#ok<ASGLU>

   % - Did we miss any deadlines?
   if (bFirst)
      bBlewBlankTime = Missed > 0;
      tEndTime = tCurrTime + tStimulusDuration;
   else
      bBlewFrameRate = bBlewFrameRate | (Missed > 0);
   end

   bFirst = false;

   if (nShowFrame == 1)
      nShowFrame = 2;
   else
      nShowFrame = 1;
   end
   
   % - Compute next flip time
   tFlipTime = tCurrTime + tFrameDuration;
end

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentOscillatingGratingStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);

% --- END of PresentOscillatingGratingStimulus.m ---

