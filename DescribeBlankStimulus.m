function [strDescription] = DescribeBlankStimulus(sStimulus, strIndent)

% BlankStimulus - STIMULUS A blank stimulus
%
% This stimulus is a grey field, presented for a requested period.
% 
% Presentation parameters:
%
%     tStimBlankTime - The desired blank duration, in seconds
%
% Stimulus parameters: None

% DescribeBlankStimulus - FUNCTION Describe the parameters for a "blank" stimulus
%
% Usage: [strDescription] = DescribeBlankStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 26th October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeBlankStimulus: Incorrect usage\n');
   disp('Usage: [strDescription] = DescribeBlankStimulus(sStimulus <, strIndent>);');
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tStimBlankTime] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});

strStimBlankTime = CheckNanSprintf('%.2f ms', [], tStimBlankTime*1e3, tStimBlankTime*1e3);

   
% -- Produce description
   
strDescription = [strIndent sprintf('Blank stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Blank period: %s\n', strStimBlankTime)];

% --- END of DescribeBlankStimulus FUNCTION ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

% --- END of DescribeBlankStimulus.m ---
