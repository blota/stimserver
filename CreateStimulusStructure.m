function [sStimulus] = CreateStimulusStructure(fhPresentationFunction, ...
                           cPresentationParameters, cStimulusArguments, fhDurationFunction, fhDescriptionFunction)

% CreateStimulusStructure - FUNCTION Package a stimulus into a stimulus structure
%
% Usage: [sStimulus] = CreateStimulusStructure(fhPresentationFunction, ...
%                            cPresentationParameters, cStimulusArguments, fhDurationFunction, fhDescriptionFunction)
%
% 'fhPresentationFunction' is a function handle to a function that has the
% calling syntax ...(hWindow, vtBlankTime, tFrameDuration, ...
%                    <presentation params list>, ...
%                    <stimulus arguments list>);
%
% 'cPresentationParameters' and 'cStimulusArguments' are cell arrays of
% arguments that will be passed to 'fhPresentationFunction'.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 30th August, 2010

if (nargin < 3)
   disp('*** CreateStimulusStructure: Incorrect usage');
   help CreateStimulusStructure;
   return;
end

if (~exist('fhDurationFunction', 'var'))
   fhDurationFunction = @(s, b)([]);
end

if (~exist('fhDescriptionFunction', 'var'))
   fhDescriptionFunction = [];
end

sStimulus.fhPresentationFunction = fhPresentationFunction;
sStimulus.cPresentationParameters = cPresentationParameters;
sStimulus.cStimulusArguments = cStimulusArguments;
sStimulus.fhDurationFunction = fhDurationFunction;
sStimulus.fhDescriptionFunction = fhDescriptionFunction;

% --- END of CreateStimulusStructure.m ---

