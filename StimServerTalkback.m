function [bSuccess] = StimServerTalkback(oTalkback)

% StimServerTalkback - FUNCTION Set talkback parameters for the stimulus server
%
% Usage: [bSuccess] = StimServerTalkback([])
%        [bSuccess] = StimServerTalkback({strTalkbackAddress nPortNumber <vnTerminator>})

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_sState; %#ok<NUSED>


% -- Check server status

if (isempty(STIMSERV_sState))
   SSlog('*** StimServerTalkback: Stimulus server not running.\n');
   bSuccess = false;
   return;
end

% - Record current talkback status
oOldTalkback = STIMSERV_sState.oTalkback;

% - Configure new talkback details
if (isempty(oTalkback))
   % - Turn off talkback
   if (~isempty(oOldTalkback))
      if (STIMSERV_sState.bUsePNET)
         pnet(STIMSERV_sState.hTalkbackSocket, 'close');
      else
         fclose(STIMSERV_sState.hTalkbackSocket);
      end

      STIMSERV_sState.hTalkbackSocket = [];
      STIMSERV_sState.oTalkback = [];
      bSuccess = true;
   end

elseif (iscell(oTalkback) && (numel(oTalkback) == 2))
   % - Try to open a TCP or UDP connection with the desired details
   
   if (STIMSERV_sState.bUsePNET)
      % - Open a connection with PNET
      
      if (STIMSERV_sState.bUseTCP)
         % - Make a TCP connection
         STIMSERV_sState.hTalkbackSocket = pnet('tcpconnect', oTalkback{1}, oTalkback{2});
         strType = 'TCP';
      else
         % - Make a UDP connection with a random source port
         STIMSERV_sState.hTalkbackSocket = pnet('udpsocket', round(rand*50000) + 1024);
         pnet(STIMSERV_sState.hTalkbackSocket, 'udpconnect', oTalkback{1}, oTalkback{2});
         strType = 'UDP';
      end
      
      % - Could a connection be made?
      if (STIMSERV_sState.hTalkbackSocket == -1)
         SSlog('--- StimServerTalkback: Warning Could not open PNET %s connection for talkback, to [%s : %d].\n', strType, oTalkback{1}, oTalkback{2});

         % - There's not much point in sending this talkback message...!
         % SStalkback('SST TALKBACK NO CONNECTION');
      end
      
      STIMSERV_sState.oTalkback = oTalkback;
      SStalkback('SST HELLO');
      bSuccess = true;
      
   else
      % - Open connetions with the instrument control toolbox
      try
         % - Create a UDP connection
         STIMSERV_sState.hTalkbackSocket = udp(oTalkback{1}, oTalkback{2});
         
         % - Turn off automatic line termination
         set(STIMSERV_sState.hTalkbackSocket, 'Terminator', '');
         
         % - Open the UDP connection
         fopen(STIMSERV_sState.hTalkbackSocket);
         STIMSERV_sState.oTalkback = oTalkback;
         
         % - Send an initial talkback message
         SStalkback('SST HELLO');
         bSuccess = true;
         
      catch errMsg
         % - We couldn't start the talkback connection
         SSlog('*** StimServerTalkback: Could not open UDP connection for talkback, to [%s : %d].\n', oTalkback{1}, oTalkback{2});
         SSlog('       Error message: %s\n', errMsg.message);
         
         % - Try to restore old talkback details
         StimServerTalkback(oOldTalkback);
         SStalkback('SST FAILED');
         bSuccess = false;
      end
   end
   
   % - Should we override the current string terminator?
   if (numel(oTalkback) > 2)
      STIMSERV_sState.vnTalkbackTerminator = oTalkback{3};
   end
   
else
   SSlog('*** StimServerTalkback: Incorrect semantics for talkback specification.\n');
   SStalkback('SST CMDERROR');
   bSuccess = false;
end



% --- END of StimServerTalkback.m ---

