%% Initialise the d��isplay
InitDisplay;

% -- Sparse noise

tFrameDuration = GetSafeFrameDurations(hWindow, 0e-3);
tPixelDuration = 1;
nNumRepeats = 1;
fUnitWidthDegrees = 10;
fUnitOverlapProportion = 0.5;
fCyclesPerDegree = 1/10;
fBarWidthDegrees = [];
fShiftCyclesPerSec = 2;
fBaseAngle = 0;
fRotateCyclesPerSec = 1;
vnNumUnits = [10 10];
vnSeed = 1:10;
nNumPixelsToPresent = [];

% sSparseNoiseFlicker = STIM_SparseNoiseFlicker(tFrame,tPixelDuration,nNumRepeats,...
%                         vfUnitSizeDegrees,vnNumUnits,fPPD,vnSeeds);
               
[sStimulus] = STIM_SparseGrating( tFrameDuration, tPixelDuration, nNumRepeats, ...
    fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPPD, ...
    fCyclesPerDegree, fBarWidthDegrees, ...
    fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
    vnSeed, nNumPixelsToPresent);

% -- Present sparse noise
PresentArbitraryStimulus(sStimulus, hWindow, 4);
sca;

