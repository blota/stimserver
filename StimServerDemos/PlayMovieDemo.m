% PlayMovieDemo.m
% 
% 
% STIM_MaskedMovie - Stimulus object
%  
%   Usage: [sStimulus] = STIM_MaskedMovie(tFrameDuration, nNumRepeats, ...
%       tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, ...
%       vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree, ...
%                                         strMovieName)

% ----------
InitDisplay;

tbaseline           = 5;      % in seconds

sBlank = STIM_Blank(tbaseline);

tFrameDuration      = GetSafeFrameDurations(hWindow, 1/25); %from InitDisplay.m
nNumRepeats         = 1;
tStimulusDuration   = 4;
bDrawMask           = true;
bInvertMask         = false;
fMaskDiameterDeg    = 100/fPPD;
vfMaskPosPix        = [0 0];
vfMovieSizeDeg      = []; %play on whole screen
vfPixelsPerDegree   = fPPD; % from InitDisplay.m
strMovieName        = 'movie184_4sec.avi';

[sFerretsShort] = STIM_MaskedMovie(tFrameDuration, nNumRepeats, ...
      tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, ...
      vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree, ...
                                        strMovieName);

sStimulus = STIM_Sequence([1,2,1], [sBlank sFerretsShort]);

PresentArbitraryStimulus(sStimulus,hWindow,0);

sca;
clear all;
% ----------
