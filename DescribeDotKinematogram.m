function [strDescription] = DescribeDotKinematogram(sStimulus, strIndent)

% DotKinematogram - STIMULUS A random dot kinematogram stimulus
%
% This stimulus is a field of shifting dots against a uniform background.  Dots are
% placed randomly in space, drift with a common speed, and have a common size.
% The entire field has a "coherence" parameter, such that a certain percentage
% of dots drift together in a common direction.  Dots are not allowed to
% overlap, nor to intersect with the window edge.  Dots also have a maximum
% persistence time.  Dots removed because they violate these constraints are
% randomly re-generated somewhere else in the field.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired frame duration, in seconds.  Use
%        'GetSafeFrameDurations' to get a value for this parameter.
%
%     tStimulusDuration - The desired duration of the entire stimulus, in
%        seconds.
%
% Stimulus parameters: 
%
%     fDotDiameterDeg - The diameter of each dot, in degrees.
%
%     fPixelsPerDegree - The calibration of the current display device.
%
%     fDotDensityPerDegreeSqr - The desired average density of dots.  This value
%        will not be exceeded over the entire display, but the dot density may
%        fall below this nominal value.
%
%     fDriftRateDegPerSec - The drift rate used for every dot, in degrees per
%        second.
%
%     tMaxPersistenceSec - The maximum persistence time that a single dot should
%        be displayed for.  This parameter will not be exceeded, but dots may
%        persist for shorter periods.
%
%     fCoherence - The desired coherence of the dot field.  This corresponds to
%        the proportion of dots that drift in a common direction.  This value is
%        not always possible to respect exactly, depending on how many dots are
%        present at a given time.  Coherence will be as close as possible to
%        this value +/- a single dot.
%
%     fCoherentDirectionRad - The desired coherent drift direction for the coherent
%        field, in radians.  '0' is to the right, 'pi/2' is downward.
%
%     vnFieldColour - This optional parameter determines the colour used for the
%     	uniform background.  Default: black.
%
%     vnDotColour - This optional parameter determines the colour used for all
%        dots.  Default: white.
%
%     nSeed - The value to be used as a random seed for the stimulus.  Using a
%        consistent seed value with consistent parameters will always generate
%        an identical stimulus.

% DescribeDotKinematogram - FUNCTION Describe the parameters for a "DotKinematogram" stimulus
%
% Usage: [strDescription] = DescribeDotKinematogram(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 2nd November, 2012

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeDotKinematogram: Incorrect usage\n');
   disp('Usage: [strDescription] = DescribeDotKinematogram(sStimulus <, strIndent>);');
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[fDotDiameterDeg, fPixelsPerDegree, ...
   fDotDensityPerDegreeSqr, fDriftRateDegPerSec, tMaxPersistenceSec, ...
   fCoherence, fCoherentDirectionRad, vnFieldColour, vnDotColour, nSeed] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});
   
strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);

strDotDiameterDeg = CheckNanSprintf('%.2f deg', [], fDotDiameterDeg, fDotDiameterDeg);
strPixelsPerDegree = CheckNanSprintf('%.2f PPD', [], fPixelsPerDegree, fPixelsPerDegree);
strDotDensityPerDegreeSqr = CheckNanSprintf('1 per %.2f deg^2', [], 1./fDotDensityPerDegreeSqr, 1./fDotDensityPerDegreeSqr);
strDriftRateDegPerSec = CheckNanSprintf('%.2f deg / s', [], fDriftRateDegPerSec, fDriftRateDegPerSec);
strMaxPersistenceSec = CheckNanSprintf('%.2f s', [], tMaxPersistenceSec, tMaxPersistenceSec);
strCoherence = CheckNanSprintf('%.2f%%', [], fCoherence*100, fCoherence*100);
strCoherentDirectionRad = CheckNanSprintf('%.2f rad', [], fCoherentDirectionRad, fCoherentDirectionRad);
strFieldColour = CheckEmptyNanSprintf(repmat('%d ', 1, numel(vnFieldColour)), '(black)', [], vnFieldColour, vnFieldColour);
strDotColour = CheckEmptyNanSprintf(repmat('%d ', 1, numel(vnFieldColour)), '(white)', [], vnDotColour, vnDotColour);
strSeed = CheckNanSprintf('%d', [], nSeed, nSeed);

   
% -- Produce description
   
strDescription = [strIndent sprintf('Random dot kinematogram stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Dot diameter: %s\n', strDotDiameterDeg) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Dot density: %s\n', strDotDensityPerDegreeSqr) ...
   strIndent sprintf('Dot drift rate: %s\n', strDriftRateDegPerSec) ...
   strIndent sprintf('Max. dot persistence time: %s\n', strMaxPersistenceSec) ...
   strIndent sprintf('Nominal dot field coherence: %s\n', strCoherence) ...
   strIndent sprintf('Coherent drift direction: %s\n', strCoherentDirectionRad) ...
   strIndent sprintf('Field colour: %s\n', strFieldColour) ...
   strIndent sprintf('Dot colour: %s\n', strDotColour) ...
   strIndent sprintf('Random seed: %s\n', strSeed)];

% --- END of DescribeBlankStimulus FUNCTION ---


function strString = CheckNanSprintf(strFormat, strNanFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strNanFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeBlankStimulus.m ---
