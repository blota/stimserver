function [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre, vfSizeDegrees] = ...
                  CalibrateXenarc7(fDistanceToScreen, vnResolution, bScalerMode)

% CalibrateMacBook - FUNCTION Returns the screen calibration information for a macbook laptop screen,
%
% Usage: [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre] = ...
%                 CalibrateXenarc7(fDistanceToScreen <, vnResolution, bScalerMode>)
%
% Calibration for a Xenarc 700yv screen
%
% 'vnResolution' optionally specifies the resolution setting for the screen.
%
% 'bScalerMode' sets whether the screen is in 16:9 mode (true; default) or 4:3
% mode (false).

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 2nd September, 2010

if (~exist('vnResolution', 'var') || isempty(vnResolution))
	vnResolution = [800 480];
end

if (~exist('bScalerMode', 'var') || isempty(bScalerMode))
   bScalerMode = true;
end

% - Determine display width
if (bScalerMode)
   % 16:9 mode; 0.152m wide
   fWidth = 0.152;
else
   % 4:3 mode; 0.122m wide
   fWidth = 0.122;
end

% -- Calibrate

fHeight = 0.0912;

[vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre, vfSizeDegrees] = ...
   CalibrateDisplay([fWidth fHeight], vnResolution, fDistanceToScreen);

% --- END of CalibrateMacBook.m ---
